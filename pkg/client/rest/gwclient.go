package rest

import (
	"bytes"
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

// GWClient holds details to connect to GW over HTTP REST
type GWClient struct {
	measureURL  string
	registerURL string
	contentType string
	client      *http.Client
}

var (
	retries        = 5
	successCounter = promauto.NewCounter(prometheus.CounterOpts{
		Name: "sensor_post_success_total",
		Help: "The total number of successful requests sent",
	})
	errorCounter = promauto.NewCounter(prometheus.CounterOpts{
		Name: "sensor_post_error_total",
		Help: "The total number of errors when posting data",
	})
)

// NewGWClient builds GWClient
func NewGWClient(host, port string) *GWClient {
	m := "http://" + host + ":" + port + "/measure"
	r := "http://" + host + ":" + port + "/register"
	return &GWClient{
		measureURL:  m,
		registerURL: r,
		contentType: "application/json",
		client:      &http.Client{},
	}
}

// PostMeasure body to our URL
func (r *GWClient) PostMeasure(body []byte) error {
	resp, err := r.post(r.measureURL, body)
	if err != nil {
		errorCounter.Inc()
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		errorCounter.Inc()
		return fmt.Errorf("gateway returned error code: %d", resp.StatusCode)
	}
	successCounter.Inc()
	return nil
}

// RegisterSensor sensor on system
func (r *GWClient) RegisterSensor(body []byte) error {
	var resp *http.Response
	var err error

	rand.Seed(time.Now().UnixNano())

	for count := 0; count < retries; count++ {
		delay := time.Duration(rand.Int63n(10))
		resp, err = r.post(r.registerURL, body)
		if err == nil {
			break
		}

		time.Sleep(delay * time.Second)
	}
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("gateway returned error code %d, when attempting to register", resp.StatusCode)
	}
	return nil
}

func (r *GWClient) post(u string, b []byte) (*http.Response, error) {
	return r.client.Post(u, r.contentType, bytes.NewBuffer(b))
}
