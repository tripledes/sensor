package sensor

import (
	"math/rand"

	"github.com/sirupsen/logrus"
)

// Sensor object for simulating a temperature reading
// sensor which sends the data by posting to an HTTP endpoint
type Sensor struct {
	GWAddress string
	GWPort    string
	R         *rand.Rand
	log       *logrus.Logger
	*Metadata
}

// Measure holds the current temperature and the timestamp at which it was taken
type Measure struct {
	Temperature float64   `json:"temperature"`
	TakenAt     string    `json:"takenAt"`
	Metadata    *Metadata `json:"metadata"`
}

// Metadata holds sensor metadata
type Metadata struct {
	ID       string    `json:"id"`
	Area     string    `json:"area"`
	Location *location `json:"location"`
}

type location struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}
