package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/kelseyhightower/envconfig"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	"gitlab.com/mdas_tfm/sensor/pkg/client"
	"gitlab.com/mdas_tfm/sensor/pkg/sensor"
)

type config struct {
	Debug       bool          `default:"false"`
	GWAddress   string        `required:"true" split_words:"true"`
	GWPort      string        `default:"8080" split_words:"true"`
	AreaName    string        `required:"true" split_words:"true"`
	GeoLoc      string        `envconfig:"location" required:"true"`
	Metrics     bool          `default:"false"`
	MetricsPort string        `default:"2112" split_words:"true"`
	Interval    time.Duration `default:"5s"`
}

// Version externally set to git tag
var Version string

func main() {
	cfg := new(config)
	err := envconfig.Process("sensor", cfg)
	if err != nil {
		log.Fatal(err)
	}

	l := logrus.New()
	l.Out = os.Stdout
	l.Formatter = &logrus.JSONFormatter{}

	if cfg.Debug {
		l.Info("enabling debug mode")
		l.Level = logrus.DebugLevel
	} else {
		l.Level = logrus.InfoLevel
	}

	l.Infof("Starting temperature sensor demo version %s", Version)
	s, err := sensor.NewSensor(
		cfg.GWAddress,
		cfg.GWPort,
		cfg.AreaName,
		cfg.GeoLoc,
		cfg.Debug,
	)
	if err != nil {
		l.Fatalf("Error creating the sensor: %v\n", err)
	}

	c, err := client.NewClient("REST", s.GWAddress, s.GWPort)
	if err != nil {
		l.Fatalf("Error creating the rest client: %v\n", err)
	}

	if cfg.Metrics {
		go func() {
			l.Infof("Starting metrics server in port %s", cfg.MetricsPort)
			mux := http.NewServeMux()
			mux.HandleFunc("/healthz", s.HealthHandler)
			mux.Handle("/metrics", promhttp.Handler())
			http.ListenAndServe(":"+cfg.MetricsPort, mux)
		}()
	}

	l.Debug("About to register the sensor")
	rb, _ := json.Marshal(s.Metadata)
	err = c.RegisterSensor(rb)
	if err != nil {
		l.Fatalf("Error registering: %v, exiting", err)
	}

	l.Debug("starting sensor loop")
	for {
		m := s.TakeSnap()
		l.Infof("Temp: %.2f", m.Temperature)
		b, err := json.Marshal(m)
		if err != nil {
			l.Error(err)
		}
		err = c.PostMeasure(b)
		if err != nil {
			l.Error(err)
		}
		l.Debugln("sleeping")
		time.Sleep(cfg.Interval)
	}
}
