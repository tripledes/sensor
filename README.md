# Sensor

[![pipeline status](https://gitlab.com/mdas_tfm/sensor/badges/master/pipeline.svg)](https://gitlab.com/mdas_tfm/sensor/-/commits/master)

## Requirements

* Golang >= 1.13

## Configuration

Sensor uses environment variables as configuration, currently supported settings:

* *SENSOR_DEBUG*
  * Enables debug logging mode. Default: False.
* *SENSOR_METRICS*
  * Enable metrics endpoint. Default: False.
* *SENSOR_METRICS_PORT*
  * Port where the metrics endpoint listens for requests. Default: 2112.
* *SENSOR_GW_ADDRESS*
  * Hostname or IP address for the destination gateway. Required.
* *SENSOR_GW_PORT*
  * TCP port to communicate with the destination gateway. Default: "8080".
* *SENSOR_AREA_NAME*
  * Area identifier where the sensor is deployed. Required.
* *LOCATION*
  * Geo location of the sensor. Required.
* *SENSOR_INTERVAL*
  * Interval at which a measurements are taken. Default: 5s.

## Building && Running

```shell
go build
export SENSOR_INTERVAL=5s
export SENSOR_GW_ADDRESS=127.0.0.1
export SENSOR_AREA_NAME=Montcada
export LOCATION=41.487222,2.187778
export SENSOR_DEBUG=true
./sensor
{"level":"info","msg":"Starting temperature sensor version 1.0.0-1-g1fb8-dirty","time":"2020-06-02T00:25:59+02:00"}
{"level":"info","msg":"\u0026{29.8944671909164 2020-06-01T22:25:59Z 0xc00009a2d0}","time":"2020-06-02T00:25:59+02:00"}
{"level":"info","msg":"Starting metrics server in port 2112","time":"2020-06-02T00:25:59+02:00"}
...
```

## Docker

```shell
export GIT_VERSION=$(git describe --abbrev=4 --dirty --always --tags)
docker build . --build-arg=VERSION=$GIT_VERSION \
  -t mdas2020/sensor:$GIT_VERSION
docker run -e SENSOR_AREA_NAME=MONTCADA -e SENSOR_GW_ADDRESS=127.0.0.1 -e SENSOR_INTERVAL=5s \
  -e LOCATION=41.487222,2.187778 -e SENSOR_METRICS=true mdas2020/sensor:$GIT_VERSION
```

## Kubernetes

```shell
cd kubernetes
export SENSOR_GW_ADDRESS=127.0.0.1
export SENSOR_AREA_NAME=Montcada
export LOCATION=41.487222,2.187778
export SENSOR_NAMESPACE=valles
./generate.sh montcada.yaml
kubectl apply -f montcada.yaml
namespace/valles created
deployment.apps/sensor created
kubectl get all -n valles
NAME                          READY   STATUS    RESTARTS   AGE
pod/sensor-58856fc86c-hbpls   0/1     Running   0          20s

NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/sensor   0/1     1            0           20s

NAME                                DESIRED   CURRENT   READY   AGE
replicaset.apps/sensor-58856fc86c   1         1         0       20s
```

