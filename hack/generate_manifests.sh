#!/bin/bash -x

set -euo pipefail


OUT_DIRECTORY=${PRD_OUT_DIRECTORY:-/tmp}
SENSOR_NAMESPACE=${PRD_SENSOR_NAMESPACE:-tfm}
CONFIGS=${PRD_CONFIGS:-../example/configurations}
CI_COMMIT_SHA=${CI_COMMIT_SHA:-deadbeef}
SENSOR_VERSION=$(git describe --abbrev=4 --dirty --always --tags)
BUILD_TIME=$(TZ=UTC date +"%Y-%m-%d - %H:%M:%S %z")
CI_PROJECT_URL=${CI_PROJECT_URL:-https://gitlab.com/mdas_tfm/sensor.git}
SENSOR_CONTAINER_IMAGE=${SENSOR_CONTAINER_IMAGE:-registry.gitlab.com/mdas_tfm/sensor}
TMPL_DIR="$(dirname "$0")/kubernetes"

export CI_COMMIT_SHA SENSOR_VERSION BUILD_TIME CI_PROJECT_URL SENSOR_CONTAINER_IMAGE

print_error() {
    printf "==> \e[31m[ERROR] ==> failed %s \e[0m\n" "${1:-'unknown'}"
    exit 1
}

namespace() {
    export SENSOR_NAMESPACE
    envsubst < "${TMPL_DIR}"/namespace.json.tmpl > "${OUT_DIRECTORY}/${SENSOR_NAMESPACE}.json"
}

deployments() {
    for sensor in $(find "${CONFIGS}"/*.sh | sed -e 's@\.sh@@' | awk -F'/' '{ print $NF }'); do
        if [ ! -f "${CONFIGS}"/"${sensor}".sh ]; then
            print_error "missing ${CONFIGS}/${sensor}.sh"
        fi

        # shellcheck disable=SC1091,SC1090
        source "${CONFIGS}/${sensor}".sh

        envsubst < "${TMPL_DIR}"/deployment.json.tmpl >> "${OUT_DIRECTORY}/kubernetes-${SENSOR_NAME}.json"
    done
}

namespace
deployments
