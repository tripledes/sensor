#!/bin/bash

set -euo pipefail
set -x

KUBERNETES_ASSETS="${CI_PROJECT_DIR}/hack/kubernetes"
ASSETS_BUILDER="${CI_PROJECT_DIR}/hack/generate_manifests.sh"
PRD_OUT_DIRECTORY="${CI_PROJECT_DIR}/../production/assets/${CI_PROJECT_NAME}"
PRD_CONFIGS="${CI_PROJECT_DIR}/../production/configurations/sensors"


print_info() {
    printf "\e[36m[INFO] ==> %s \e[0m\n" "${1:-'unknown'}"
}

print_ok() {
    printf "\e[32m[SUCCESS] ==> %s \e[0m\n" "${1:-'unknown'}"
}

print_error() {
    printf "==> \e[31m[ERROR] ==> failed %s \e[0m\n" "${1:-'unknown'}"
    exit 1
}

if [ ! -d "${KUBERNETES_ASSETS}" ]; then
    print_error "Could not find Kubernetes assets in ${KUBERNETES_ASSETS}"
fi

if [ ! -f "${ASSETS_BUILDER}" ]; then
    print_error "Could not find Kubernetes assets builder script"
fi

if [ ! -d "${PRD_OUT_DIRECTORY}" ]; then
    print_info "Creating ${PRD_OUT_DIRECTORY} directory ..."
    mkdir -p "${LOCAL_ASSETS}"
fi

CURRENT=$(find "${PRD_OUT_DIRECTORY}"/*.json | wc -l)

if [ "${CURRENT}" -ne 0 ]; then
    rm "${PRD_OUT_DIRECTORY}"/*.json || true
fi

export PRD_OUT_DIRECTORY PRD_CONFIGS

print_info "Generating kubernetes assets ..."
${ASSETS_BUILDER} || print_error "to generate kubernetes resources"

print_ok "Assets for ${CI_PROJECT_NAME} successfully created in ${PRD_OUT_DIRECTORY}"

exit 0
